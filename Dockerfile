# Create a new image to build the production code

FROM node:dubnium-alpine as builder
WORKDIR /app
COPY . .
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++
RUN npm ci
RUN npm run build

# Build final image for production

FROM nginx:alpine
COPY --from=builder /app/dist /usr/share/nginx/html
COPY --from=builder /app/nginx.conf /etc/nginx/conf.d/default.conf
