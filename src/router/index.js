import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/components/HomePage'
import About from '@/components/About'
import Article from '@/components/Article'
import Error404 from '@/components/Error404'
import vueSmoothScroll from 'vue-smooth-scroll'

Vue.use(Router)
Vue.use(vueSmoothScroll)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/about',
      name: 'About',
      component: About,
    },
    {
      path: '/article/:id',
      name: 'article',
      component: Article,
    },
    {
      path: '*',
      name: 'fallback-404',
      component: Error404,
    }
  ]
})
