// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'

Vue.config.productionTip = false
Vue.use(Vuex);

const baseStorageUri = "https://storage.gra.cloud.ovh.net/v1/AUTH_0697890e15ad4098b140de1fed916171/maudgourdon.com";
const store = new Vuex.Store({
  state: {
    baseStorageUri,
    MenuOrder: [
      15,
      12,
      13,
      14,
      11,
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
    ],
    homepage: [
      {id: 15, image: 1},
      {id: 12, image: 1},
      {id: 8, image: 3},
      {id: 2, image: 2},
      {id: 11, image: 4},
      {id: 15, image: 15},
      {id: 6, image: 1},
      {id: 6, image: 7},
      {id: 3, image: 3},
      {id: 14, image: 2},
      {id: 15, image: 5},
      {id: 10, image: 1},
      {id: 6, image: 2},
      {id: 10, image: 2},
      {id: 11, image: 8},
      {id: 3, image: 5},
      {id: 15, image: 7},
      {id: 11, image: 5},
      {id: 6, image: 3},
      {id: 8, image: 2},
      {id: 9, image: 3},
      {id: 13, image: 1},
      {id: 15, image: 6},
      {id: 6, image: 4},
      {id: 3, image: 2},
    ],
    articles: [
      {
        id: 1,
        title: 'Ciel vide',
        date: 2018,
        long_description: `Mural painting`,
        images: [
          "/images/CV_1.png",
          "/images/CV_2.png",
        ],
      },
      {
        id: 2,
        title: 'Grâce au travail de ceux qui espèrent',
        date: 2018,
        long_description: `series of drawings about friendship`,
        images: [
          "/images/GATDCQP_1.png",
          "/images/GATDCQP_2.png",
          "/images/GATDCQP_3.png",
          "/images/GATDCQP_4.png",
        ],
      },
      {
        id: 3,
        title: 'Protocole for Unexpected Dancer',
        date: '2017-2018',
        long_description: `seven pop songs and seven weather forecasts`,
        images: [
          "/images/PfUD_1.png",
          "/images/PfUD_2.png",
          "/images/PfUD_3.png",
          "/images/PfUD_4.png",
          "/images/PfUD_5.png",
        ],
      },
      {
        id: 4,
        title: 'BM&Y',
        date: 2017,
        long_description: `six offended love letters`,
        images: [
          "/images/BMY_1.png",
          "/images/BMY_2.png",
          "/images/BMY_3.png",
        ],
      }, 
      {
        id: 5,
        title: 'M-587',
        date: 2017,
        long_description: `performance for two readers based on the score of a weaving pattern`,
        extended: true,
        images: [
          "/images/M587_1.png",
        ],
      }, 
      {
        id: 6,
        title: "Peinée l'eau peut",
        date: 2017,
        long_description: `visual song distorting the pattern of Ulysses’s and Penelope’s character and love story, as well as the notion of rhythm and time`,
        images: [
          "/images/PEP_1.png",
          "/images/PEP_2.png",
          "/images/PEP_3.png",
          "/images/PEP_4.png",
          "/images/PEP_5.png",
          "/images/PEP_6.png",
          "/images/PEP_7.png",
          "/images/PEP_8.png",
        ],
      }, 
      {
        id: 7,
        title: "Seven Songs",
        date: 2017,
        long_description: `seven pop songs and seven weather forecasts`,
        image_count: 4,
        images: [
          "/images/7S_1.png",
          "/images/7S_2.png",
          "/images/7S_3.png",
          "/images/7S_4.png",
        ]
      }, 
      {
        id: 8,
        title: "A.MonSeulDésir.R",
        date: 2016,
        long_description: `eighteen flowers vinyl stickers and a frieze using a pattern derived from the Jacquard knitting technique.`,
        images: [
          "/images/AMSDR_1.png",
          "/images/AMSDR_2.png",
          "/images/AMSDR_3.png",
          "/images/AMSDR_4.png",
          "/images/AMSDR_5.png",
        ]
      }, 
      {
        id: 9,
        title: "EXOACTO",
        date: 2016,
        long_description: `series of drawings with an appel and a knife`,
        extended: true,
        images: [
          "/images/Exoacto_1.png",
          "/images/Exoacto_2.png",
          "/images/Exoacto_3.png",
          "/images/Exoacto_4.png",
        ]
      }, 
      {
        id: 10,
        title: "Sun is raising twice",
        date: 2015,
        long_description: `series of drawing with a window and a rose`,
        images: [
          "/images/Sunisraisingtwice_1.png",
          "/images/Sunisraisingtwice_2.png",
          "/images/Sunisraisingtwice_3.png",
          "/images/Sunisraisingtwice_4.png",
        ]
      },
      {
        id: 11,
        title:"A Flower is Speaking to a Dog",
        date: 2019,
        description: "",
        long_description: `<p>A Flower is Speaking to a Dog is the result of Gourdon’s fascination with and 
        study of DNA as a form within which a language can be built.</p>
        <p>The publication consists of a set of generative texts in which the genetic sequence of DNA functions 
        as an underlying structure or a partite for the characters.</p><br />
        Picture credits: <i>Rachel Gruijters</i> & <i>Leontien Allemeersch</i>`,
        images: [
          "/images/BOEKS_1.png",
          "/images/BOEKS_2.png",
          "/images/BOEKS_3.png",
          "/images/BOEKS_4.png",
          "/images/BOEKS_5.png",
          "/images/BOEKS_6.png",
          "/images/BOEKS_7.png",
          "/images/BOEKS_8.png",
          "/images/BOEKS_9.png",
          "/images/BOEKS_10.png",
          "/images/BOEKS_11.png",
          "/images/BOEKS_12.png",
          "/images/BOEKS_13.png",
        ],
      },
      {
        id: 12,
        title: "Pains Surprises",
        date: 2019,
        description: "Surprised Pains or Bread on the Power Socket",
        long_description: `The fictional texts developed in Pains Surprises strive to be untranslatable, as it is 
        precisely when tongues and languages slip that fictions start. Here, the language is used as an active 
        substance - a « pharmakon » : an ambivalent environment where nothing is what it is expected and where 
        everything is in transformation, in translation.`,
        images: [
          "/images/PS_1.png",
          "/images/PS_2.png",
          "/images/PS_3.png",
          "/images/PS_4.jpeg",
        ]
      },
      {
        id: 13,
        title: "Domestic Echo",
        date: 2019,
        description: "",
        long_description: `<p>Projet in collaboration with Chantal van Rijt. The exhibition renders the idea of 
        an interconnected universe where data is used as a departure point for robotic tales and poetry. 
        Via simple mechanisms of loops and repetitions, Chantal van Rijt and Maud Gourdon generate visual and 
        linguistic scores through which objects, machines and humans interact in a joyful and choreographed chaos.</p>
        
        <p><a class="underlined" href="${baseStorageUri}/files/DomesticEcho_KoiPersyn.pdf" target="_BLANK">Full text about the show by Koi Persyn</a><br />
        credit photo : Chantal van Rijt</p>`,
        images: [
          "/images/DE_1.png",
          "/images/DE_2.png",
          "/images/DE_3.png",
          "/images/DE_4.png",
          "/images/DE_5.png",
          "/images/DE_6.png",
          "/images/DE_7.png",
          "/images/DE_8.png",
          "/images/DE_9.png",
        ]
      },
      {
        id: 14,
        title: "A good-talking candle",
        description: "",
        long_description: `<p>Reading in group by candlelight in the context of BOEKS 04 : A Flower is Speaking to Dog. 
        A good-talking candle is an invitation to dive into a selection of stories about multispecies. 
        This assemblage of texts from various sources claim a desire for transformation by the use of 
        fiction as a reservoir for thought experiments.</p><br />
        
        <p>credit photo : Rachel Gruijters</p>`,
        images: [
          "/images/AGTC_2.png",
          "/images/AGTC_1.png",
        ]
      },
      {
        id: 15,
        title: "Therolinguistic Tale, a verse of wormdots and seedlines",
        description: "",
        long_description: `<p>Project with the artist Chantal van Rijt.<br />
        During a five-month residency, Maud Gourdon and Chantal van Rijt explored the concepts of coding and decoding. Working with traces of beetles and dried seeds they attempted to decipher messages of tunnels, holes and dots. In the exhibition, Maud and Chantal consider plants and animals as our linguistic equals as they search for ways to translate their hidden and discarded language.</p>
        <a style='text-decoration: underline;' href="${baseStorageUri}/files/TherolinguisticTale/Interview_M_Magazine.pdf" target="_BLANK">Interview M-Magazine</a><br />
        photo credits : Chantal van Rijt`,
        images: [
          "/images/Therolinguist_1.jpg",
          "/images/Therolinguist_2.jpg",
          "/images/Therolinguist_3.jpg",
          "/images/Therolinguist_4.jpg",
          "/images/Therolinguist_5.jpg",
          "/images/Therolinguist_6.jpg",
          "/images/Therolinguist_7.jpg",
          "/images/Therolinguist_8.jpg",
          "/images/Therolinguist_9.jpg",
          "/images/Therolinguist_10.jpg",
          "/images/Therolinguist_11.jpg",
          "/images/Therolinguist_12.jpg",
          "/images/Therolinguist_13.jpg",
          "/images/Therolinguist_14.jpg",
          "/images/Therolinguist_15.jpg",
          "/images/Therolinguist_16.jpg",
        ]
      },
    ],
  },
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
